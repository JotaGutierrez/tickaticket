import json


class Composition:
    def __init__(self, product = None):
        self.id = None
        self.product = product
        self._lines = None

    def set_lines(self, lines):
        self._lines = lines

    def lines(self):
        if self._lines is None:
            return [['Producto', '']]
        return self._lines

    def from_row(self, row):

        self.id = row[0]
        self._lines = [['Producto', '']]
        try:
            self._lines = self.clean_lines(json.loads(row[1]))
        except ValueError:
            pass

        return self

    def to_array(self):
        return [
            self.id,
            self._lines,
        ]

    def edit(self, id, lines):
        self.id = id
        self._lines = self.clean_lines(lines)

    def clean_lines(self, lines):
        return [line for line in lines if line[0] != '' and line[1] != '']
