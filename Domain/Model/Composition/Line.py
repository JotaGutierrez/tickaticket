
class Line:

    def __init__(self, label="", line_text=""):
        self.id = None
        self.label = label
        self.line_text = line_text
        pass

    def edit(self, label, line_text):
        self.label = label
        self.line_text = line_text

    def to_array(self):
        return [
            self.id,
            self.label,
            self.line_text,
        ]

    def from_line(self, line):
        self.id = line[0]
        self.label = line[1]
        self.line_text = line[2]