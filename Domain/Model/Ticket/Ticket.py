class Ticket:
    def __init__(self, title = None):
        self.id = None
        self.title = title

        self.product = ""
        self.variety = ""
        self.origin = ""
        self.category = ""
        self.caliber = ""
        self.lot = ""
        self.consumption = ""
        self.weight = ""
        self.ingredients = ""

        self.product_label = ""
        self.variety_label = ""
        self.origin_label = ""
        self.category_label = ""
        self.caliber_label = ""
        self.lot_label = ""
        self.consumption_label = ""
        self.weight_label = ""
        self.ingredients_label = ""

    def from_row(self, row):
        print(row)
        self.id = row[0]
        self.product_label = row[1]
        self.product = row[2]
        self.variety_label = row[3]
        self.variety = row[4]
        self.origin_label = row[5]
        self.origin = row[6]
        self.category_label = row[7]
        self.category = row[8]
        self.caliber_label = row[9]
        self.caliber = row[10]
        self.lot_label = row[11]
        self.lot = row[12]
        # self.consumption_label = row[13]
        # self.consumption = row[14]
        # self.weight_label = row[15]
        # self.weight = row[16]
        # self.ingredients_label = row[17]
        # self.ingredients = row[18]

        return self

    def to_array(self):
        return [
            self.id,

            self.product_label,
            self.product,
            self.variety_label,
            self.variety,
            self.origin_label,
            self.origin,
            self.category_label,
            self.category,
            self.caliber_label,
            self.caliber,
            self.lot_label,
            self.lot,
            # self.consumption_label,
            # self.consumption,
            # self.weight_label,
            # self.weight,
            # self.ingredients_label,
            # self.ingredients
        ]

    def edit(self, row):
        self.from_row(row)
        