#!/usr/bin/python
# -*- coding: utf-8 -*-
# @Author: fry
# @Date:   2017-06-30 10:15:26
# @Last Modified by:   fry
# @Last Modified time: 2017-06-30 13:03:25

import os
import sys
import subprocess

from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from optparse import OptionParser

from PyQt5.QtWidgets import QApplication, qApp, QDialog

from Infrastructure.Persistence.Db import db_instance
from Infrastructure.UI.PrintPreview import PrintPreview
from Infrastructure.UI.TickaComposition import TickaComposition
from Infrastructure.UI.Tickatick import Tickatick


class App(QtWidgets.QWidget):

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.layout = QtWidgets.QVBoxLayout()
        self.db = db_instance
        self.composition_widget = TickaComposition(self.db)
        self.ticket_widget = Tickatick(self.db)
        self.tabs = QtWidgets.QTabWidget()
        self.print_preview = PrintPreview()

        self.tool_bar = QtWidgets.QToolBar()
        self.exit_action = QtWidgets.QAction("Salir", self)
        self.update_action = QtWidgets.QAction("Actualizar", self)

        self.init_ui()

        self.exit_action.triggered.connect(self.exit)
        self.update_action.triggered.connect(self.update_app)
        self.ticket_widget.ticket_selected.connect(self.update_ticket_preview)
        self.composition_widget.composition_selected.connect(self.update_composition_preview)

    @staticmethod
    def update_app():
        process = subprocess.Popen("./bash/update.sh")
        process.wait()
        args = sys.argv[:]
        args.insert(0, sys.executable)
        os.execv(sys.executable, args)

    @staticmethod
    def exit():
        qApp.quit()

    def update_ticket_preview(self):
        self.print_preview.add_top_pixmap(self.ticket_widget.ticket_widget.get_print_pixmap())

    def update_composition_preview(self):
        self.print_preview.add_bottom_pixmap(self.composition_widget.composition_widget.get_print_pixmap())

    def init_ui(self):
        spacer = QtWidgets.QWidget()
        self.tool_bar.setMaximumHeight(22)
        spacer.setSizePolicy(QtWidgets.QSizePolicy().Expanding, QtWidgets.QSizePolicy().Ignored)
        self.tool_bar.addWidget(spacer)
        self.tool_bar.addAction(self.update_action)
        self.tool_bar.addAction(self.exit_action)
        self.tool_bar.setGeometry(self.width(), 0, self.width(), 5)

        self.tabs.setContentsMargins(0,0,0,0)
        self.tabs.addTab(self.ticket_widget, "Tickets")
        self.tabs.addTab(self.composition_widget, "Composicion")
        self.tabs.setMaximumHeight(360)
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.addWidget(self.tool_bar)
        self.layout.addWidget(self.tabs)
        self.layout.addWidget(self.print_preview)
        self.setLayout(self.layout)

        #self.setGeometry(300, 300, 800, 880)
        #self.setMinimumSize(830, 650)
        #self.setMaximumSize(830, 650)
        self.center()
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


def main(argv):
    parser = OptionParser()
    parser.add_option("-k", "--key", dest="key", action="store", default="c",
                      help="Starting key of the progression")
    parser.add_option("-m", "--mode", dest="mode", action="store", default="M",
                      help="major (M) or minor(m)")
    parser.add_option("-s", "--steps", dest="steps", action="store", default="4",
                      help="number of iterations")

    (options, args) = parser.parse_args()

    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    palette = QtGui.QPalette()
    palette.setColor(QtGui.QPalette.Window, QtGui.QColor(53, 53, 53))
    palette.setColor(QtGui.QPalette.WindowText, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Base, QtGui.QColor(15, 15, 15))
    palette.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(53, 53, 53))
    palette.setColor(QtGui.QPalette.ToolTipBase, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.ToolTipText, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Text, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.Button, QtGui.QColor(53, 53, 53))
    palette.setColor(QtGui.QPalette.ButtonText, QtCore.Qt.white)
    palette.setColor(QtGui.QPalette.BrightText, QtCore.Qt.red)

    palette.setColor(QtGui.QPalette.Highlight, QtGui.QColor(142, 45, 197).lighter())
    palette.setColor(QtGui.QPalette.HighlightedText, QtCore.Qt.black)

    tick = App()

    sys.exit(app.exec_())


if __name__ == "__main__":
    main(sys.argv[1:])
