
import json
from Application.Query.Composition import Queries
from Domain.Model.Composition.Composition import Composition
from Infrastructure.Persistence.Db import db_instance


class CompositionRepository:

    def __init__(self):
        self.db_manager = db_instance
        pass

    def composition_of_id(self, composition_id):
        composition = self.db_manager.get_by_id(Queries.GET_BY_ID_QUERY, composition_id)
        composition = Composition().from_row(composition)
        return composition

    def create(self, composition):
        self.db_manager.execute(Queries.INSERT_QUERY, [json.dumps(composition.lines())])

    def save(self, composition):
        self.db_manager.execute(Queries.EDIT_QUERY, [json.dumps(composition.lines()), composition.id])

    def remove(self, composition):
        pass

    def remove_composition_of_id(self, id):
        self.db_manager.execute(Queries.DELETE_QUERY, [id])

    def query(self, query=None):
        pass
