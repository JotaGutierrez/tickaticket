import sqlite3
from PyQt5 import QtCore

from PyQt5.QtCore import pyqtSignal

from Application.Query.Ticket.Queries import TICKET_CREATE
from Application.Query.Composition.Queries import COMPOSITION_CREATE
from Infrastructure.Persistence.Migrations.Migrations import migrations


class Db(QtCore.QObject):

    db_changed = pyqtSignal()

    def __init__(self, db_name):
        QtCore.QObject.__init__(self)
        self.conn = sqlite3.connect(db_name)
        self.init_db()
        self.migrate_db()

    def migrate_db(self):
        c = self.conn.cursor()

        for query in migrations:
            try:
                c.execute(query)
            except sqlite3.OperationalError:
                pass    # Ignore sql errors

        self.conn.commit()

    def init_db(self):
        try:
            c = self.conn.cursor()
            c.execute(TICKET_CREATE)
            self.conn.commit()
            c.execute(COMPOSITION_CREATE)
            self.conn.commit()
            c.close()
        except sqlite3.Error as e:
            print(e)

    def get_all(self, query):
        c = self.conn.cursor()
        c.execute(query)
        result = c.fetchall()
        return result

    def get_by_id(self, query, id):
        c = self.conn.cursor()
        c.execute(query, (id,))
        row = c.fetchone()
        return row

    def delete(self, query, id):
        c = self.conn.cursor()
        try:
            c.execute(query, (id,))
            self.conn.commit()
            c.close()
            self.db_changed.emit()
        except sqlite3.Error as e:
            print(e)
            return False

        return True

    def execute(self, query, params):
        c = self.conn.cursor()
        try:
            c.execute(query, params)
            self.conn.commit()
            c.close()
            self.db_changed.emit()
        except sqlite3.Error as e:
            print(e)
            return False

        return True


db_instance = Db('./ticket-db.sqlite')
