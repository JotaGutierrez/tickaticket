migrations = [
    'ALTER TABLE composition ADD COLUMN lines text DEFAULT "";',
    'ALTER TABLE tickets ADD COLUMN product_label VARCHAR(255) DEFAULT NULL;',
    'ALTER TABLE tickets ADD COLUMN variety_label VARCHAR(255) DEFAULT NULL;',
    'ALTER TABLE tickets ADD COLUMN origin_label VARCHAR(255) DEFAULT NULL;',
    'ALTER TABLE tickets ADD COLUMN category_label VARCHAR(255) DEFAULT NULL;',
    'ALTER TABLE tickets ADD COLUMN caliber_label VARCHAR(255) DEFAULT NULL;',
    'ALTER TABLE tickets ADD COLUMN lot_label VARCHAR(255) DEFAULT NULL;',
    'ALTER TABLE tickets ADD COLUMN consumption_label VARCHAR(255) DEFAULT NULL;',
    'ALTER TABLE tickets ADD COLUMN weight_label VARCHAR(255) DEFAULT NULL;',
    'ALTER TABLE tickets ADD COLUMN ingredients_label VARCHAR(255) DEFAULT NULL;',
]
