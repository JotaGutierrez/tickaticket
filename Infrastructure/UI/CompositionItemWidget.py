from PyQt5 import QtWidgets, QtCore


class CompositionItemWidget(QtWidgets.QWidget):

    def __init__(self, composition):
        QtWidgets.QWidget.__init__(self)
        self.line_wrappers = [QtWidgets.QHBoxLayout() for i in range(len(composition.lines()))]
        self.top_wrapper = QtWidgets.QVBoxLayout()
        self.composition = composition
        self.edit = None
        self.delete = None
        self.setMinimumSize(350, 190)
        self.init_ui()

    def init_ui(self):
        self.edit = QtWidgets.QPushButton("editar")

        wrapper = QtWidgets.QVBoxLayout()
        self.top_wrapper.setAlignment(QtCore.Qt.AlignTop)

        bottom_wrapper = QtWidgets.QHBoxLayout()

        for i, line in enumerate(self.line_wrappers):
            lbl = QtWidgets.QLabel(self.composition.lines()[i][0])
            line.addWidget(lbl)
            lbl_txt = QtWidgets.QLabel(str(self.composition.lines()[i][1]))
            line.addWidget(lbl_txt)
            i += 1
            self.top_wrapper.addLayout(line)

        bottom_wrapper.addWidget(self.edit)
        self.edit.show()

        wrapper.addLayout(self.top_wrapper)
        wrapper.addLayout(bottom_wrapper)

        self.setLayout(wrapper)

    def print_version(self):
        self.line_wrappers[0].itemAt(0).widget().setText(u'Composicion')
        self.line_wrappers[0].itemAt(1).widget().setText(u'por 100g.')
        self.top_wrapper.setSpacing(0)
        self.top_wrapper.setContentsMargins(0, 0, 0, 0)
        self.top_wrapper.setStretch(0, 0)
        for layout in self.line_wrappers:
            layout.setSpacing(0)
            layout.setContentsMargins(0, 0, 0, 0)
            layout.setStretch(4, 0)

        self.edit.setParent(None)
        self.setStyleSheet('''
                    QWidget{
                        background-color:white;
                        color:black;
                        font-size:10pt;
                        margin-top:0;
                        margin-bottom:0;
                    }
                    QHBoxLayout {
                        height:6pt;
                        margin-top:0;
                        margin-bottom:0;
                        padding:0;
                        spacing:0;
                    }
                    QPushButton {
                        border: none;
                        background: none;
                    }
                ''')
        self.setMinimumSize(400, 0)
        self.adjustSize()

        return self
