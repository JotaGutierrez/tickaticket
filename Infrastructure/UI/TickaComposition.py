from PyQt5 import QtWidgets, QtCore

from PyQt5.QtCore import pyqtSlot, pyqtSignal

from Application.Query.Composition import Queries
from Domain.Model.Composition.Composition import Composition
from Infrastructure.UI.CompositionEditWidget import CompositionEditWidget
from Infrastructure.UI.CompositionItemWidget import CompositionItemWidget
from Infrastructure.Persistence.ORM.Composition.CompositionRepository import CompositionRepository


class TickaComposition(QtWidgets.QWidget):

    composition_selected = pyqtSignal()

    def clear_items(self):
        for i in range(self.item_list.count()):
            self.item_list.itemAt(i).widget().deleteLater()

    @pyqtSlot(name = "UPDATEUI")
    def update_ui(self):
        self.clear_items()
        result = self.db.get_all(Queries.GET_ALL_QUERY)
        for i in range(len(result)):
            self.add_item(result[i])

    def add_item(self, item):
        composition = self.composition_repository.composition_of_id(item[0])
        widget = CompositionItemWidget(composition)
        widget.edit.clicked.connect(lambda: self.item_edit(composition.id))
        self.item_list.addWidget(widget)

    def item_edit(self, item_id):
        row = self.db.get_by_id(Queries.GET_BY_ID_QUERY, item_id)
        composition = Composition().from_row(row)
        self.composition_widget.clear()
        self.composition_widget.load_composition(composition)
        self.composition_selected.emit()

    def item_delete(self, item_id):
        button_reply = QtWidgets.QMessageBox.question(self, 'Borrar Composition', "Quieres borrar el composition?",
                                                      QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                      QtWidgets.QMessageBox.No)
        if button_reply == QtWidgets.QMessageBox.Yes:
            self.db.delete(item_id)
            self.update_ui()

    def add_composition(self):
        self.add_composition_panel(self.main_panel)

    def __init__(self, db):
        super(TickaComposition, self).__init__()

        self.db = db
        self.db.db_changed.connect(self.update_ui)
        self.composition_repository = CompositionRepository()
        self.main_container = QtWidgets.QHBoxLayout()
        self.main_panel = QtWidgets.QVBoxLayout()
        self.item_list_scrollarea = QtWidgets.QScrollArea()
        self.item_list_wrapper = QtWidgets.QWidget()
        self.item_list = QtWidgets.QVBoxLayout()
        self.add_button = QtWidgets.QPushButton("Nuevo")

        self.item_list_scrollarea.setGeometry(QtCore.QRect(5, 5, 390, 550))
        self.item_list_scrollarea.setMinimumSize(390, 280)
        self.item_list_scrollarea.setMaximumSize(390, 280)
        self.item_list_wrapper.setLayout(self.item_list)
        self.item_list_scrollarea.setWidgetResizable(True)
        self.item_list_scrollarea.setWidget(self.item_list_wrapper)
        self.main_panel.addWidget(self.item_list_scrollarea)

        self.add_button.clicked.connect(self.add_composition)

        self.main_panel.addWidget(self.add_button)

        self.add_button.show()

        self.main_container.addLayout(self.main_panel)
        self.composition_widget = CompositionEditWidget(self.main_container)
        self.composition_widget.set_db(self.db)

        self.setLayout(self.main_container)
        self.setGeometry(300, 300, 800, 800)
        self.setMinimumSize(800, 330)
        self.setMaximumSize(800, 330)
        self.show()

        self.composition_list = []

        self.update_ui()

    def add_composition_panel(self, widget):
        self.composition_widget.clear()
        composition = Composition()
        self.composition_widget.load_composition(composition)

    def close_event(self):
        self.delete_layout(self.panel)

    def delete_layout(self, layout):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.clearLayout(item.layout())
