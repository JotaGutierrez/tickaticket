from PyQt5 import QtWidgets, QtPrintSupport, QtGui, QtCore

from PyQt5.QtPrintSupport import QPrinter, QPrintDialog


class PrintPreview(QtWidgets.QWidget):

    def __init__(self, pixmap = None):
        QtWidgets.QWidget.__init__(self)
        self.pixmap = pixmap
        self.vbox = QtWidgets.QVBoxLayout()
        self.hbox = QtWidgets.QHBoxLayout()

        self.print_wrapper = QtWidgets.QHBoxLayout()
        self.print_size = QtWidgets.QDoubleSpinBox()
        self.print_size.setDecimals(1)
        self.print_size.setValue(10.0)
        self.print_text = QtWidgets.QLineEdit("1")
        self.print_button = QtWidgets.QPushButton("Imprimir")

        self.top_label = QtWidgets.QLabel()
        self.bottom_label = QtWidgets.QLabel()

        self.top_pixmap = None
        self.bottom_pixmap = None

        self.set_ui()
        self.show()

    def set_ui(self):
        self.print_size.setMaximumWidth(100)
        self.print_wrapper.addWidget(self.print_size)
        self.print_wrapper.addWidget(self.print_text)
        self.print_wrapper.addWidget(self.print_button)
        self.print_button.clicked.connect(self._print)

        self.setMinimumSize(800, 150)
        self.setMaximumSize(800, 150)

        if self.pixmap is not None:
            self.top_label.setPixmap(self.pixmap)
            self.top_label.resize(self.pixmap.width(), self.pixmap.height())

        self.hbox.addWidget(self.top_label)
        self.hbox.addWidget(self.bottom_label)

        self.vbox.addLayout(self.hbox)
        self.vbox.addLayout(self.print_wrapper)

        self.setLayout(self.vbox)
        self.top_label.show()
        self.bottom_label.show()
        self.resize(self.top_label.width() + self.bottom_label.width(),
                    self.top_label.height() + self.bottom_label.height())

    def add_pixmap_to_label(self, pixmap, label):
        label.setPixmap(pixmap)
        label.resize(pixmap.width(), pixmap.height())

    def add_top_pixmap(self, pixmap):
        if pixmap is not None:
            self.add_pixmap_to_label(pixmap, self.top_label)
            self.top_pixmap = pixmap

    def add_bottom_pixmap(self, pixmap):
        if pixmap is not None:
            self.add_pixmap_to_label(pixmap, self.bottom_label)
            self.bottom_pixmap = pixmap

    def get_total_width(self):
        width = 0
        if self.bottom_pixmap is not None:
            width += self.bottom_pixmap.width()
        if self.top_pixmap is not None:
            width += self.top_pixmap.width()
        return width
    
    def get_total_height(self):
        height = 0
        if self.bottom_pixmap is not None:
            height += self.bottom_pixmap.height()
        if self.top_pixmap is not None:
            height += self.top_pixmap.height()
        return height

    def _print(self):

        print_count = self.print_text.text()
        printer = QPrinter(QPrinter.HighResolution)
        printer.setCopyCount(int(print_count))
        printer.setPageMargins(0, 0, 0, 0, QPrinter.Millimeter)

        transform = QtGui.QTransform().rotate(90)

        top_pixmap = None
        if self.top_pixmap is not None:
            top_pixmap = self.top_pixmap
            top_pixmap = top_pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)

        bottom_pixmap = None
        if self.bottom_pixmap is not None:
            bottom_pixmap = self.bottom_pixmap
            bottom_pixmap = bottom_pixmap.transformed(transform, QtCore.Qt.SmoothTransformation)

        # Create painter
        painter = QtGui.QPainter()
        # Start painter
        painter.begin(printer)
        # Grab a widget you want to print

        if float(self.print_size.text().replace(',', '.')) > 0:
            scale = float(self.print_size.text().replace(',', '.'))
        else:
            scale_x = printer.pageRect().width() / self.get_total_width()
            scale_y = printer.pageRect().height() / self.get_total_height()
            scale = min(scale_x, scale_y)

        painter.scale(scale, scale)
        if bottom_pixmap is not None:
            painter.drawPixmap(0, 0, bottom_pixmap)

        if top_pixmap is not None:
            if bottom_pixmap is not None:
                painter.drawPixmap(bottom_pixmap.width(), 0, top_pixmap)
            else:
                painter.drawPixmap(0, 0, top_pixmap)

        # End painting
        painter.end()
