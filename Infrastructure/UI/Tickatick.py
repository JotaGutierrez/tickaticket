from PyQt5 import QtWidgets, QtCore

from PyQt5.QtCore import pyqtSlot, pyqtSignal

from Application.Query.Ticket import Queries
from Domain.Model.Ticket.Ticket import Ticket
from Infrastructure.UI.TicketEditWidget import TicketEditWidget
from Infrastructure.UI.TicketItemWidget import TicketItemWidget


class Tickatick(QtWidgets.QWidget):
    ticket_selected = pyqtSignal()

    def clear_items(self):
        for i in range(self.item_list.count()):
            self.item_list.itemAt(i).widget().deleteLater()

    @pyqtSlot(name="UPDATEUI")
    def update_ui(self):
        self.clear_items()
        result = self.db.get_all(Queries.GET_ALL_QUERY)
        for i in range(len(result)):
            self.add_item(result[i])

    def add_item(self, item):
        ticket = Ticket().from_row(self.db.get_by_id(Queries.GET_TICKET_BY_ID_QUERY, item[0]))
        widget = TicketItemWidget(ticket)
        widget.edit.clicked.connect(lambda: self.item_edit(ticket.id))
        self.item_list.addWidget(widget)

    def item_edit(self, item_id):
        row = self.db.get_by_id(Queries.GET_TICKET_BY_ID_QUERY, item_id)
        ticket = Ticket().from_row(row)
        self.ticket_widget.load_ticket(ticket)
        self.ticket_selected.emit()

    def item_delete(self, item_id):
        button_reply = QtWidgets.QMessageBox.question(self, 'Borrar Ticket', "Quieres borrar el ticket?",
                                                      QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                      QtWidgets.QMessageBox.No)
        if button_reply == QtWidgets.QMessageBox.Yes:
            self.db.delete(item_id)
            self.update_ui()

    def add_ticket(self):
        self.add_ticket_panel(self.main_panel)

    def __init__(self, db):
        super(Tickatick, self).__init__()

        self.db = db
        self.db.db_changed.connect(self.update_ui)

        self.main_container = QtWidgets.QHBoxLayout()

        self.main_panel = QtWidgets.QVBoxLayout()
        self.item_list_scrollarea = QtWidgets.QScrollArea()
        self.item_list_scrollarea.setGeometry(QtCore.QRect(5, 5, 390, 300))
        self.item_list_scrollarea.setMinimumSize(390, 280)
        self.item_list_scrollarea.setMaximumSize(390, 280)
        self.item_list_wrapper = QtWidgets.QWidget()
        self.item_list = QtWidgets.QVBoxLayout()
        self.item_list_wrapper.setLayout(self.item_list)
        self.item_list_scrollarea.setWidgetResizable(True)
        self.item_list_scrollarea.setWidget(self.item_list_wrapper)
        self.main_panel.addWidget(self.item_list_scrollarea)

        self.add_button = QtWidgets.QPushButton("Nuevo")
        self.add_button.clicked.connect(self.add_ticket)

        self.main_panel.addWidget(self.add_button)

        self.add_button.show()

        self.main_container.addLayout(self.main_panel)
        self.ticket_widget = TicketEditWidget(self.main_container)
        self.ticket_widget.set_db(self.db)

        self.setLayout(self.main_container)
        self.setGeometry(300, 300, 800, 800)
        self.setMinimumSize(800, 330)
        self.setMaximumSize(800, 330)
        self.show()

        self.ticket_list = []

        self.update_ui()

    def add_ticket_panel(self, widget):
        self.ticket_widget.clear()

    def close_event(self):
        self.delete_layout(self.panel)

    def delete_layout(self, layout):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.clearLayout(item.layout())
