from PyQt5 import QtWidgets, QtPrintSupport, QtGui, QtCore

from PyQt5.QtPrintSupport import QPrinter, QPrintDialog

from Application.Query.Ticket import Queries
from Domain.Model.Ticket.Ticket import Ticket
from Infrastructure.UI.PrintPreview import PrintPreview
from Infrastructure.UI.TicketItemWidget import TicketItemWidget


class TicketEditWidget():
    FIELDS = [
        ["product", "Producto"],
        ["variety", "Variedad", "print_variety"],
        ["origin", "Origen", "print_origin"],
        ["category", "Categoria", "print_category"],
        ["caliber", "Calibre", "print_caliber"],
        ["lot", "Lote", "print_lot"],
        ["consumption", "Consumo", "print_consumption"],
        ["weight", "Peso", "print_weight"],
        ["ingredients", "Ingredientes", "print_ingredients"],
    ]

    LINES = 6

    def __init__(self, parent):
        self.print_text = QtWidgets.QLineEdit("1")
        self.print_button = QtWidgets.QPushButton("Imprimir")
        self.preview_button = QtWidgets.QPushButton("Ver")
        self.print_layout = QtWidgets.QHBoxLayout()
        self.delete_button = QtWidgets.QPushButton("borrar")
        self.save_button = QtWidgets.QPushButton("guardar")
        self.button_layout = QtWidgets.QHBoxLayout()
        self.parent = parent
        self.form = QtWidgets.QVBoxLayout()
        self.set_ui()
        self.id = None
        self.p = None

    def set_db(self, db):
        self.db = db

    def set_ui(self):
        for i in range(6):
            self.add_field()

        self.button_layout.addWidget(self.save_button)
        self.button_layout.addWidget(self.delete_button)

        self.print_layout.addWidget(self.print_text)
        self.print_layout.addWidget(self.preview_button)
        self.print_layout.addWidget(self.print_button)

        self.form.addLayout(self.button_layout)
        self.form.addStretch(0)
        # self.form.addLayout(self.print_layout)
        self.parent.addLayout(self.form)

        self.save_button.clicked.connect(self.save_ticket)
        self.delete_button.clicked.connect(self.item_delete)
        self.preview_button.clicked.connect(self.preview_ticket)
        self.print_button.clicked.connect(self.print_ticket)

    def add_field(self):
        lay = QtWidgets.QHBoxLayout()
        lbl = QtWidgets.QLineEdit()
        txt = QtWidgets.QLineEdit()
        lay.addWidget(lbl)
        lay.addWidget(txt)
        self.form.addLayout(lay)

    def item_delete(self):
        button_reply = QtWidgets.QMessageBox.question(self.delete_button, 'Borrar Ticket', "Quieres borrar el ticket?",
                                                      QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                      QtWidgets.QMessageBox.No)
        if button_reply == QtWidgets.QMessageBox.Yes:
            self.db.execute(Queries.DELETE_TICKET_QUERY, (self.id,))
            self.id = None

    def load_ticket(self, ticket):
        self.id = ticket.id
        for i in range(6):
            self.form \
                .itemAt(i) \
                .itemAt(0) \
                .widget() \
                .setText(ticket.to_array()[((i+1) * 2) - 1])
            self.form \
                .itemAt(i) \
                .itemAt(1) \
                .widget() \
                .setText(ticket.to_array()[(i+1) * 2])

    def save_ticket(self):
        ticket = Ticket()

        values = [item for sublist in
                  [[self.form
                    .itemAt(i)
                    .itemAt(0)
                    .widget()
                    .text(),
                    self.form
                    .itemAt(i)
                    .itemAt(1)
                    .widget()
                    .text()] for i in range(self.LINES)]
                  for item in sublist
                  ]

        ticket.edit([self.id] + values)

        if self.id is None:
            self.db.execute(Queries.INSERT_TICKET_QUERY, ticket.to_array()[1:])
        else:
            self.db.execute(Queries.EDIT_TICKET_QUERY, ticket.to_array()[1:] + [ticket.id])

    def delete_ticket(self):
        self.db.delete(Queries.DELETE_TICKET_QUERY, self.id)
        self.id = None

    def clear(self):
        self.id = None
        for i in range(self.LINES):
            self.form \
                .itemAt(i) \
                .itemAt(0) \
                .widget() \
                .setText("")
            self.form \
                .itemAt(i) \
                .itemAt(1) \
                .widget() \
                .setText("")

    def print_ticket(self):
        if self.id is None:
            return

        print_count = self.print_text.text()
        printer = QPrinter(QPrinter.HighResolution)
        printer.setCopyCount(int(print_count))
        dlg = QPrintDialog(printer)

        dlg.setWindowTitle("Print Document")

        screen = self.get_print_pixmap()

        if dlg.exec_() == QPrintDialog.Accepted:
            # Create painter
            painter = QtGui.QPainter()
            # Start painter
            painter.begin(printer)
            # Grab a widget you want to print

            scale_x = printer.pageRect().width() / screen.width()
            scale_y = printer.pageRect().height() / screen.height()
            scale = min(scale_x, scale_y)
            painter.scale(scale, scale)

            painter.drawPixmap(0, 0, screen)

            # End painting
            painter.end()

        del dlg

    def preview_ticket(self):
        screen = self.get_print_pixmap()

        self.p = PrintPreview(screen)
        self.p.show()

    def get_print_pixmap(self):
        if self.id is not None:
            widget = TicketItemWidget(
                Ticket().from_row(self.db.get_by_id(Queries.GET_TICKET_BY_ID_QUERY, self.id))
            ).print_version()
            screen = widget.grab()
            transform = QtGui.QTransform().rotate(0)
            return screen.transformed(transform, QtCore.Qt.SmoothTransformation)
        return None
