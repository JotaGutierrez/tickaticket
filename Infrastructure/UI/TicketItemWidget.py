from PyQt5 import QtWidgets, QtCore


class TicketItemWidget(QtWidgets.QWidget):

    LABELS = [
        "Producto",
        "Variedad",
        "Origen",
        "Cat",
        "Cal",
        "Lote/f.Envasado",
        "Cons. Preferente",
        "Peso Neto",
        "Ingredientes",
        "Envasado por Hnos.Gutierrez",
        "Ctra de Velez nº17, Benamargosa, Malaga"
    ]

    def __init__(self, ticket):
        QtWidgets.QWidget.__init__(self)
        self.top_wrapper = QtWidgets.QVBoxLayout()
        self.line_wrappers = [QtWidgets.QHBoxLayout() for i in range(8)]
        self.ticket = ticket
        self.edit = None
        self.delete = None
        self.setMinimumSize(350, 190)
        self.init_ui()

    def init_ui(self):
        self.edit = QtWidgets.QPushButton("editar")

        wrapper = QtWidgets.QVBoxLayout()
        self.top_wrapper.setAlignment(QtCore.Qt.AlignTop)

        bottom_wrapper = QtWidgets.QHBoxLayout()

        index = 0

        for line in self.line_wrappers:
            if index < 12:
                lbl = QtWidgets.QLabel(str(self.ticket.to_array()[index+1]))
                line.addWidget(lbl)
                lbl_txt = QtWidgets.QLabel(str(self.ticket.to_array()[index+2]))
                line.addWidget(lbl_txt)

                self.top_wrapper.addLayout(line)

                index += 2

        line = self.line_wrappers[6]
        line.addWidget(QtWidgets.QLabel("Envasado por Hnos.Gutierrez"))
        self.top_wrapper.addLayout(line)

        line = self.line_wrappers[7]
        line.addWidget(QtWidgets.QLabel("Ctra de Velez nº17, Benamargosa, Malaga"))
        self.top_wrapper.addLayout(line)

        bottom_wrapper.addWidget(self.edit)
        self.edit.show()

        wrapper.addLayout(self.top_wrapper)
        wrapper.addLayout(bottom_wrapper)

        self.setLayout(wrapper)

    def print_version(self):
        self.top_wrapper.setSpacing(0)
        self.top_wrapper.setContentsMargins(0, 0, 0, 0)
        self.top_wrapper.setStretch(0, 0)
        for layout in self.line_wrappers:
            layout.setSpacing(0)
            layout.setContentsMargins(0, 0, 0, 0)
            layout.setStretch(0, 0)

        self.edit.setParent(None)
        self.setStyleSheet('''
                    QWidget{
                        background-color:white;
                        color:black;
                        font-size:10pt;
                        margin-top:0;
                        margin-bottom:0;
                    }
                    QHBoxLayout {
                        height:6pt;
                        margin-top:0;
                        margin-bottom:0;
                        padding:0;
                        spacing:0;
                    }
                    QPushButton {
                        border: none;
                        background: none;
                    }
                ''')
        self.setMinimumSize(400, 0)
        self.adjustSize()

        return self
