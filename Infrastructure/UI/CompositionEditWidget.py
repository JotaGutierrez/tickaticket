from PyQt5 import QtWidgets, QtPrintSupport, QtGui, QtCore

import sip
from PyQt5.QtPrintSupport import QPrinter, QPrintDialog

from Application.Query.Composition import Queries
from Domain.Model.Composition.Composition import Composition
from Infrastructure.UI.CompositionItemWidget import CompositionItemWidget
from Infrastructure.UI.PrintPreview import PrintPreview
from Infrastructure.Persistence.ORM.Composition.CompositionRepository import CompositionRepository


class CompositionEditWidget():

    def __init__(self, parent):
        self.parent = parent
        self.print_text = QtWidgets.QLineEdit("1")
        self.print_button = QtWidgets.QPushButton("Imprimir")
        self.preview_button = QtWidgets.QPushButton("Ver")
        self.print_layout = QtWidgets.QHBoxLayout()
        self.add_button = QtWidgets.QPushButton("+")
        self.delete_button = QtWidgets.QPushButton("Borrar")
        self.save_button = QtWidgets.QPushButton("Guardar")
        self.button_layout = QtWidgets.QHBoxLayout()
        self.form = QtWidgets.QVBoxLayout()
        self.text_panel = QtWidgets.QVBoxLayout()
        self.db = None
        self.set_ui()
        self.id = None
        self.p = None
        self.composition_repository = CompositionRepository()

    def set_db(self, db):
        self.db = db

    def set_ui(self):
        self.form.addLayout(self.text_panel)
        self.button_layout.addWidget(self.add_button)
        self.button_layout.addWidget(self.save_button)
        self.button_layout.addWidget(self.delete_button)

        self.print_layout.addWidget(self.print_text)
        self.print_layout.addWidget(self.preview_button)
        self.print_layout.addWidget(self.print_button)

        self.form.addLayout(self.button_layout)
        self.form.addStretch(0)
        #self.form.addLayout(self.print_layout)
        self.parent.addLayout(self.form)

        self.add_button.clicked.connect(self.add_line)
        self.save_button.clicked.connect(self.save_composition)
        self.delete_button.clicked.connect(self.item_delete)
        self.preview_button.clicked.connect(self.preview_composition)
        self.print_button.clicked.connect(self.print_composition)

    def add_line(self):
        self.add_field()

    def add_field(self):
        lay = QtWidgets.QHBoxLayout()
        lbl = QtWidgets.QLineEdit()
        lbl.setFixedWidth(100)
        txt = QtWidgets.QLineEdit()
        lay.addWidget(lbl)
        lay.addWidget(txt)
        self.text_panel.addLayout(lay)

    def item_delete(self):
        button_reply = QtWidgets.QMessageBox.question(self.delete_button, 'Borrar Composicion', "Quieres borrar la lista?",
                                                      QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                      QtWidgets.QMessageBox.No)
        if button_reply == QtWidgets.QMessageBox.Yes:
            self.composition_repository.remove_composition_of_id(self.id)
            self.id = None

    def load_composition(self, composition):
        self.id = composition.id
        for i, line in enumerate(composition.lines()):
            self.add_field()
            self.text_panel \
                .itemAt(i) \
                .itemAt(0) \
                .widget() \
                .setText(line[0])
            self.text_panel \
                .itemAt(i) \
                .itemAt(1) \
                .widget() \
                .setText(line[1])

    def save_composition(self):
        composition = Composition()
        composition.edit(self.id,
            [
                [
                    self.text_panel
                    .itemAt(i)
                    .itemAt(0)
                    .widget()
                    .text(),
                    self.text_panel
                    .itemAt(i)
                    .itemAt(1)
                    .widget()
                    .text()
                ] for i in range(self.text_panel.count())
            ])

        if self.id is None:
            self.composition_repository.create(composition)
        else:
            self.composition_repository.save(composition)

    def delete_composition(self):
        self.db.delete(Queries.DELETE_QUERY, self.id)

    def clear(self):
        self.id = None

        for i in reversed(range(self.text_panel.count())):
            lay = self.text_panel.itemAt(i)

            for item in reversed(range(lay.count())):
                widget = lay.itemAt(item).widget()
                if widget is not None:
                    self.text_panel.itemAt(i).itemAt(item).widget().deleteLater()
            sip.delete(self.text_panel.itemAt(i))

    def print_composition(self):
        if self.id is None:
            return

        print_count = self.print_text.text()
        printer = QPrinter(QPrinter.HighResolution)
        printer.setCopyCount(int(print_count))
        dlg = QPrintDialog(printer)

        dlg.setWindowTitle("Print Document")

        screen = self.get_print_pixmap()

        if dlg.exec_() == QPrintDialog.Accepted:
            # Create painter
            painter = QtGui.QPainter()
            # Start painter
            painter.begin(printer)
            # Grab a widget you want to print
            scale_x = printer.pageRect().width() / screen.width()
            scale_y = printer.pageRect().height() / screen.height()
            scale = min(scale_x, scale_y)
            painter.scale(scale, scale)
            painter.drawPixmap(0, 0, screen)

            # End painting
            painter.end()

        del dlg

    def preview_composition(self):
        screen = self.get_print_pixmap()

        self.p = PrintPreview(screen)
        self.p.show()

    def get_print_pixmap(self):
        if self.id is not None:
            widget = CompositionItemWidget(
                Composition().from_row(self.db.get_by_id(Queries.GET_BY_ID_QUERY, self.id))
            ).print_version()
            screen = widget.grab()
            transform = QtGui.QTransform().rotate(0)
            return screen.transformed(transform, QtCore.Qt.SmoothTransformation)

        return None
