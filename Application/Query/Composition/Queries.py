COMPOSITION_CREATE = '''
    Create table if not exists composition(
        id INTEGER PRIMARY KEY,
        product VARCHAR(255) DEFAULT NULL,
        cals VARCHAR(255) DEFAULT NULL,
        hidrate VARCHAR(255) DEFAULT NULL,
        protein VARCHAR(255) DEFAULT NULL,
        fibre VARCHAR(255) DEFAULT NULL,
        potasio VARCHAR(255) DEFAULT NULL,
        iron VARCHAR(255) DEFAULT NULL,
        mag VARCHAR(255) DEFAULT NULL,
        calc VARCHAR(255) DEFAULT NULL
    );
'''

GET_ALL_QUERY = '''
    SELECT id, lines FROM composition;
'''

INSERT_QUERY = '''
    INSERT INTO composition(
      lines
    ) values (?)
'''

EDIT_QUERY = '''
    UPDATE composition SET  
        lines = ?
    WHERE id = ?
'''

DELETE_QUERY = '''
    DELETE FROM composition WHERE id = ?
'''

GET_BY_ID_QUERY = '''
    SELECT id, lines FROM composition WHERE id = ?
'''
