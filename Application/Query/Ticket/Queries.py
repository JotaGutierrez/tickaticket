TICKET_CREATE = '''
    Create table if not exists tickets(
        id INTEGER PRIMARY KEY,
        title VARCHAR(255) DEFAULT NULL,
        product VARCHAR(255) DEFAULT NULL,
        variety VARCHAR(255) DEFAULT NULL,
        origin VARCHAR(255) DEFAULT NULL,
        category VARCHAR(255) DEFAULT NULL,
        caliber VARCHAR(255) DEFAULT NULL,
        lot VARCHAR(255) DEFAULT NULL,
        consumption VARCHAR(255) DEFAULT NULL,
        weight VARCHAR (255) DEFAULT NULL,
        ingredients VARCHAR (255) DEFAULT NULL,
        print_variety INT DEFAULT 1,
        print_origin INT DEFAULT 1,
        print_category INT DEFAULT 1,
        print_caliber INT DEFAULT 1,
        print_lot INT DEFAULT 1,
        print_consumption INT DEFAULT 1,
        print_weight INT DEFAULT 1,
        print ingredients INT DEFAULT 1
    );
'''

GET_ALL_QUERY = '''
    SELECT * FROM tickets;
'''

INSERT_TICKET_QUERY = '''
    INSERT INTO tickets(
      product_label,
      product, 
      variety_label,
      variety, 
      origin_label,
      origin, 
      category_label,
      category, 
      caliber_label,
      caliber,
      lot_label,
      lot
    ) values (?, ?, ?, ?, ?, ?, ?, ?, ?,
    ?, ?, ?)
'''

EDIT_TICKET_QUERY = '''
    UPDATE tickets SET
        product_label = ?,
        product = ?,
        variety_label = ?,
        variety = ?,
        origin_label = ?,
        origin = ?,
        category_label = ?,
        category = ?,
        caliber_label = ?,
        caliber = ?,
        lot_label = ?,
        lot = ?
    WHERE id = ?
'''

GET_TICKET_BY_ID_QUERY = '''
    SELECT 
      id,
      product_label,
      product, 
      variety_label,
      variety, 
      origin_label,
      origin, 
      category_label,
      category, 
      caliber_label,
      caliber,
      lot_label,
      lot
   FROM tickets WHERE id = ?
'''

DELETE_TICKET_QUERY = '''
    DELETE FROM tickets WHERE id = ?
'''