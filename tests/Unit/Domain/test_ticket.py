from Domain.Model.Ticket.Ticket import Ticket


def test_instantiation():
    assert isinstance(Ticket(), Ticket), 'Ticket instantiation OK'


def test_hydrating_from_row():
    row = [1,
           'product_label', 'product',
           'variety_label', 'variety',
           'origin_label', 'origin',
           'category_label', 'category',
           'caliber_label', 'caliber',
           'lot_label', 'lot',
           ]

    ticket = Ticket()
    ticket.from_row(row)

    assert ticket.product_label == 'product_label', 'product label is OK'
    assert ticket.product == 'product', 'product is OK'
    assert ticket.variety_label == 'variety_label', 'variety label is OK'
    assert ticket.variety == 'variety', 'variety is OK'
    assert ticket.origin_label == 'origin_label', 'origin label is OK'
    assert ticket.origin == 'origin', 'origin is OK'
    assert ticket.category_label == 'category_label', 'category label is OK'
    assert ticket.category == 'category', 'category is OK'
    assert ticket.caliber_label == 'caliber_label', 'caliber label is OK'
    assert ticket.caliber == 'caliber', 'caliber is OK'
    assert ticket.lot_label == 'lot_label', 'lot label is OK'
    assert ticket.lot == 'lot', 'caliber is OK'


def test_converting_to_array():
    row = [1,
           'product_label', 'product',
           'variety_label', 'variety',
           'origin_label', 'origin',
           'category_label', 'category',
           'caliber_label', 'caliber',
           'lot_label', 'lot',
           ]

    ticket = Ticket()
    ticket.from_row(row)

    assert ticket.to_array() == [1,
                                 'product_label', 'product',
                                 'variety_label', 'variety',
                                 'origin_label', 'origin',
                                 'category_label', 'category',
                                 'caliber_label', 'caliber',
                                 'lot_label', 'lot',
                                 ], 'product array is OK'


def test_edit():
    row = [1,
           'product_label', 'product',
           'variety_label', 'variety',
           'origin_label', 'origin',
           'category_label', 'category',
           'caliber_label', 'caliber',
           'lot_label', 'lot',
           ]

    ticket = Ticket()
    ticket.from_row(row)

    edit_row = [1,
                'product_label_edit', 'product_edit',
                'variety_label_edit', 'variety_edit',
                'origin_label_edit', 'origin_edit',
                'category_label_edit', 'category_edit',
                'caliber_label_edit', 'caliber_edit',
                'lot_label_edit', 'lot_edit',
                ]
    ticket.edit(edit_row)
    assert ticket.to_array() == [1,
                                 'product_label_edit', 'product_edit',
                                 'variety_label_edit', 'variety_edit',
                                 'origin_label_edit', 'origin_edit',
                                 'category_label_edit', 'category_edit',
                                 'caliber_label_edit', 'caliber_edit',
                                 'lot_label_edit', 'lot_edit',
                                 ], 'product array is OK'
