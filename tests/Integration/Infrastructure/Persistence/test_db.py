from Application.Query.Ticket import Queries
from Domain.Model.Ticket.Ticket import Ticket
from Infrastructure.Persistence.Db import Db

db = Db('./db.test.sqlite')


def test_create():
    row = [1,
           'product_label', 'product',
           'variety_label', 'variety',
           'origin_label', 'origin',
           'category_label', 'category',
           'caliber_label', 'caliber',
           'lot_label', 'lot',
           ]

    ticket = Ticket()
    ticket.from_row(row)

    assert True is db.execute(Queries.INSERT_TICKET_QUERY, ticket.to_array()[1:]), 'It should insert a new Ticket'


def test_get_all():
    result = db.get_all(Queries.GET_ALL_QUERY)

    assert 1 == len(result), 'It should return existing Ticket'


def test_get_by_id():
    row = db.get_by_id(Queries.GET_TICKET_BY_ID_QUERY, 1)

    assert 1 == row[0], 'Returned id should be 1'
    assert (1,
            'product_label',
            'product',
            'variety_label',
            'variety',
            'origin_label',
            'origin',
            'category_label',
            'category',
            'caliber_label',
            'caliber',
            'lot_label',
            'lot',
            ) == row, 'Returned row should equals saved one'


def test_edit():
    row = db.get_by_id(Queries.GET_TICKET_BY_ID_QUERY, 1)
    ticket = Ticket().from_row(row)
    ticket.from_row([1,
                     'product_label_edit', 'product_edit',
                     'variety_label_edit', 'variety_edit',
                     'origin_label_edit', 'origin_edit',
                     'category_label_edit', 'category_edit',
                     'caliber_label_edit', 'caliber_edit',
                     'lot_label_edit', 'lot_edit',
                     ])

    db.execute(Queries.EDIT_TICKET_QUERY, ticket.to_array()[1:] + [ticket.id])

    row = db.get_by_id(Queries.GET_TICKET_BY_ID_QUERY, 1)

    assert 1 == row[0], 'Returned id should remain 1'
    assert (1,
            'product_label_edit',
            'product_edit',
            'variety_label_edit',
            'variety_edit',
            'origin_label_edit',
            'origin_edit',
            'category_label_edit',
            'category_edit',
            'caliber_label_edit',
            'caliber_edit',
            'lot_label_edit',
            'lot_edit',
            ) == row, 'Returned row should equals saved one'


def test_delete():
    assert True is db.delete(Queries.DELETE_TICKET_QUERY, 1), 'It should delete existing Ticket'
